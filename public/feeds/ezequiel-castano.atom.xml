<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"><title>Ezequiel Leonardo Castaño Personal Website - Ezequiel Castaño</title><link href="https://elc.github.io/" rel="alternate"></link><link href="http://feeds.feedburner.com/feeds/ezequiel-castano.atom.xml" rel="self"></link><id>https://elc.github.io/</id><updated>2018-02-24T00:00:00-03:00</updated><entry><title>Create one executable file for a Flask app with PyInstaller</title><link href="https://elc.github.io/posts/executable-flask-pyinstaller" rel="alternate"></link><published>2018-02-24T00:00:00-03:00</published><updated>2018-02-24T00:00:00-03:00</updated><author><name>Ezequiel Castaño</name></author><id>tag:elc.github.io,2018-02-24:/posts/executable-flask-pyinstaller</id><summary type="html">&lt;p&gt;After trying lots of solutions from stack overflow, quora and several blogs without success I decided to post how I managed to make one single file executable with Flask and PyInstaller&lt;/p&gt;
</summary><content type="html">&lt;p&gt;After trying lots of solutions from stack overflow, quora and several blogs without success I decided to post how I managed to make one single file executable with Flask and PyInstaller&lt;/p&gt;


&lt;p&gt;If you just want to read the solution, scroll down to the solution section or clic &lt;a href="#solution"&gt;here&lt;/a&gt;&lt;/p&gt;
&lt;h1 id="whats-exactly-the-problem"&gt;&lt;a class="toclink" href="#whats-exactly-the-problem"&gt;What's exactly the problem?&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Well, Flask is a WEB framework, used to create WEBsites that will be served by a WEB server and view from a WEB browser. Problems emerge when we try to replace 'WEB' with desktop in the previous sentence, we are trying to force something built for a particular scenario with its limitations to work in a complely different one.&lt;/p&gt;
&lt;h1 id="reasoning-about-it"&gt;&lt;a class="toclink" href="#reasoning-about-it"&gt;Reasoning about it&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;So, why on Earth someone would want such a thing, I mean, web apps are cool, trendy and they are everywhere isn't it? Yes, it's true but it's also true that when you want to share something with someone you want to be as smooth as possible, imagine these 2 cases:&lt;/p&gt;
&lt;p&gt;Case 1:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Hey! I built some amazing piece of software!&lt;/li&gt;
&lt;li&gt;Really?! Send me a copy so I could check it out&lt;/li&gt;
&lt;li&gt;Sure! Just extract the file in your email and execute the file inside&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Case 2:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Hey! I built some amazing piece of software!&lt;/li&gt;
&lt;li&gt;Really?! Send me a copy so I could check it out&lt;/li&gt;
&lt;li&gt;Perfect! But it won't work unless you do some things before, you have install Python 3, make sure the arquitechture is properly selected according to your operating system, then create a virtual environment so you don't make a mess with the libraries, then install with pip (look for a tutorial in youtube if you don't know how) all the libraries in the requirements.txt file, oh, sure, you have to clone (what's that?) the repository to have access to all the files, and finally run in the console/terminal &lt;code&gt;python app.py&lt;/code&gt; if you get an error probably I you have some incompatibilities, send me a screenshot so I can send you the wheel to the library causing the problem... ... ...&lt;/li&gt;
&lt;li&gt;... Maybe another time&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;So yes, it's a bit exagerated but for illustration purposes it's fine. Desktop frameworks in Python aren't its strength so people usually tend to use web frameworks and use the browser as the "window". That's not bad at all, there are lots of reasons why web frameworks are better than desktop alternatives (versioning, updates, better security, etc..). However, these pieces of software are not self-contained and in some (maybe a lot) cases it's almost necessary to have self-contained applications&lt;/p&gt;
&lt;p&gt;Some of the main advantages of the self-contained (also known as portable) software are:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;It isn't necessary to keep a virtual environment with the specific versions of all the dependencies&lt;/li&gt;
&lt;li&gt;It isn't necessary to know which software is installed in the destination machine.&lt;/li&gt;
&lt;li&gt;It isn't necessary to install anything at all, just copy and paste or execute from removable drive.&lt;/li&gt;
&lt;li&gt;It is way more clear to the final user where is the program and how to execute it&lt;/li&gt;
&lt;li&gt;It is harder (yet not impossible) to reverse engineer the software&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;There are some disadvantages obviously such as:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Since all dependencies are included, files could get BIG ( Hello World app in Flask is about 25MB)&lt;/li&gt;
&lt;li&gt;Version control is useless on self-contained files&lt;/li&gt;
&lt;li&gt;If you consume self-contained software you trust your provider about its safety&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;This are some points to consider, they could be advantages or disadvantages depending on the case&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;You treat the software as a black box&lt;/li&gt;
&lt;li&gt;When a new version is released you just replace the file and you're done&lt;/li&gt;
&lt;/ul&gt;
&lt;h1 id="flask-comes-into-the-action"&gt;&lt;a class="toclink" href="#flask-comes-into-the-action"&gt;Flask comes into the action&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;If you attempt to port a web app to a desktop one without changing the framework chances are you will use a lightweigth framework like Flask. Now, how do you create a desktop app? Using a desktop framework (Qt, Tk, wx, etc), the most commonly used for this task is Qt. The idea is to create a minimal web browser capable of rendering HTML and then, execute the flask application server and browse to the url of the server inside this browser.&lt;/p&gt;
&lt;p&gt;But what are the differences between creating a web browser and using the systems default? Well, first of all, you assume there will be one, and that that one would be able to render all the HTML, CSS and JS you are using, that could not be the case. More often than not we found ourselves developing software for old operating systems (aka Windows XP o older).&lt;/p&gt;
&lt;p&gt;Sounds easy and actually it can be done, I was contributing to a script to achieve this very goal &lt;a href="https://github.com/smoqadam/PyFladesk"&gt;PyFladesk&lt;/a&gt;. There are some concerns about which version of Qt is the appropiate and the convenience of one over the other.&lt;/p&gt;
&lt;h1 id="the-problem"&gt;&lt;a class="toclink" href="#the-problem"&gt;The problem&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Until now there is no problem, but when you try to bundle everything into one single file problems arise&lt;/p&gt;
&lt;p&gt;What kind of problem could occur? Each item in the list is an example:&lt;/p&gt;
&lt;h3 id="stack-overflow"&gt;&lt;a class="toclink" href="#stack-overflow"&gt;Stack Overflow&lt;/a&gt;&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://stackoverflow.com/questions/32149892/flask-application-built-using-pyinstaller-not-rendering-index-html"&gt;Flask application built using pyinstaller not rendering index.html - jinja2.exceptions.TemplateNotFound&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://stackoverflow.com/questions/47832309/pyinstaller-on-flask-app-import-error"&gt;Pyinstaller on Flask app, import error&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://stackoverflow.com/questions/47018930/using-pyinstaller-on-python-flask-application-to-create-executable"&gt;Using Pyinstaller on Python Flask Application to create Executable&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://stackoverflow.com/questions/40191441/python-2-7-12-trying-to-build-an-executable-file-using-pyinstaller-i-keep-gett"&gt;Python 2.7.12, trying to build an executable file using pyinstaller. I keep getting the below error&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id="quora"&gt;&lt;a class="toclink" href="#quora"&gt;Quora&lt;/a&gt;&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.quora.com/Can-I-convert-a-Flask-application-into-an-executable-file-that-runs-on-Windows-like-an-exe-file"&gt;Can I convert a Flask application into an executable file that runs on Windows like an .exe file?&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id="reddit"&gt;&lt;a class="toclink" href="#reddit"&gt;Reddit&lt;/a&gt;&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.reddit.com/r/Python/comments/21evjn/is_it_possible_to_deploydistribute_flask_as_an/"&gt;Is it possible to deploy/distribute Flask as an executable for desktop use?&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id="blogs"&gt;&lt;a class="toclink" href="#blogs"&gt;Blogs&lt;/a&gt;&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="http://mapopa.blogspot.com.ar/2013/10/flask-and-pyinstaller-notice.html"&gt;Flask and pyinstaller notice&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h1 id="solution"&gt;&lt;a class="toclink" href="#solution"&gt;Solution&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Note: If all you use is flask for served static or pseudo static content you could tried &lt;a href="http://pythonhosted.org/Frozen-Flask/"&gt;Frozen Flask&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;After reading all the previous posts and some of the &lt;a href="https://pythonhosted.org/PyInstaller/"&gt;PyInstaller docs&lt;/a&gt;. I found that some people actually solved it! But, the solution they propose was editing the spec file, which is generated after a first run of PyInstaller. I thought that solution was a hack and not the proper way to achieve what I wanted. &lt;/p&gt;
&lt;p&gt;So I tried to understand what the changes in the spec file did and it turned out that that changes was to copy the folders Flask uses into the file directory/file (Actually one of the proposed solutions was build and then copy paste the folders, but besides being unpractical it wouldn't work with one file builds). so I search in the docs for a command line argument to achieve the same, there is none, well, actually there is, but it isn't in the docs page.&lt;/p&gt;
&lt;p&gt;So after researching a bit I came with following solution (Explanation below):&lt;/p&gt;
&lt;p&gt;Windows &lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;pyinstaller -w -F --add-data &amp;quot;templates;templates&amp;quot; --add-data &amp;quot;static;static&amp;quot; app.py
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Linux (NOT TESTED)&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;pyinstaller -w -F --add-data &amp;quot;templates:templates&amp;quot; --add-data &amp;quot;static:static&amp;quot; app.py
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;This will create a folder &lt;code&gt;dist&lt;/code&gt; with our executable ready to be shipped. The executable will open the main window of our app.&lt;/p&gt;
&lt;p&gt;When a first used this was for a contribution for the &lt;a href="https://github.com/smoqadam/PyFladesk"&gt;PyFladesk&lt;/a&gt; project and I realize that since Qt is quite big, our executables were big too. The example app of that repository is 70 MB (much of which was the Qt Component for displaying HTML (WebEngine)). This is reasonable taking into account that we were shipping a self contain web browser.&lt;/p&gt;
&lt;h2 id="tutorial"&gt;&lt;a class="toclink" href="#tutorial"&gt;Tutorial&lt;/a&gt;&lt;/h2&gt;
&lt;p&gt;If you haven't already, install it with pip (if you use virtual environments you should install it inside it)&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;pip install pyinstaller
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Some parameters to consider:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;code&gt;F&lt;/code&gt; - Bundles everything in a single file&lt;/li&gt;
&lt;li&gt;&lt;code&gt;w&lt;/code&gt; - Avoid displaying a console&lt;/li&gt;
&lt;li&gt;&lt;code&gt;--add-data&lt;/code&gt; - Add Folders to the directory/executable&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Since Flask relies on a directory structure you should pass it to the folder, in the example case we only have two folders: &lt;code&gt;templates&lt;/code&gt; and &lt;code&gt;static&lt;/code&gt;, in case you use a database or some other directory structure you may adapt this.&lt;/p&gt;
&lt;p&gt;Note: For more complex scenarios check the &lt;a href="https://pythonhosted.org/PyInstaller/usage.html"&gt;PyInstaller Docs&lt;/a&gt;&lt;/p&gt;
&lt;h1 id="the-other-problem-the-size"&gt;&lt;a class="toclink" href="#the-other-problem-the-size"&gt;The other problem: The size&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Is the project using as few dependencies as possible? If yes, continue reading, if not check and then come back. Make sure you create a virtual environment for your project and execute PyInstaller from there, if the size is still big, I recommend you to check one of these:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Use Virtual Environments and install everything you need there, including PyInstaller (but nothing more!)&lt;/li&gt;
&lt;li&gt;Check if all your dependences are really necessary, try to use the standard library when possible&lt;/li&gt;
&lt;li&gt;Check if your biggest dependencies could be replaced with lightweight alternatives&lt;/li&gt;
&lt;li&gt;Use one-dir option and then see what are the biggest dlls and if you can exclude them&lt;/li&gt;
&lt;li&gt;Use the &lt;a href="http://pyinstaller.readthedocs.io/en/stable/advanced-topics.html#using-pyi-archive-viewer"&gt;ArchiveViewer.py script&lt;/a&gt; that's part of PyInstaller and exclude everything you don't need&lt;/li&gt;
&lt;/ul&gt;
&lt;h1 id="conclusion"&gt;&lt;a class="toclink" href="#conclusion"&gt;Conclusion&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Using PyInstaller and Flask is not as hard as people may experience if you have the correct knowledge but it requires a bit of work to get a clean, lightweight file. However, it's possible to create executable with complex apps, with special directory structure, databases and so on, but don't expect that to be a tiny file.&lt;/p&gt;</content><category term="Python"></category><category term="Flask"></category><category term="PyInstaller"></category></entry><entry><title>Haskell pattern matching in Python</title><link href="https://elc.github.io/posts/haskell-pattern-matching-in-python" rel="alternate"></link><published>2018-02-18T00:00:00-03:00</published><updated>2018-02-18T00:00:00-03:00</updated><author><name>Ezequiel Castaño</name></author><id>tag:elc.github.io,2018-02-18:/posts/haskell-pattern-matching-in-python</id><summary type="html">&lt;p&gt;I started to learn a bit of Haskell and one of its features amazed me: Pattern Matching. Then, unexpectably, I notice Python 3 have them too but with a different name: Extended Tuple Unpacking. Okay, it's not exactly the same but you can get quite similar functionalities if you master it. This post is inspired by this &lt;a href="https://stackoverflow.com/questions/6967632/unpacking-extended-unpacking-and-nested-extended-unpacking"&gt;question&lt;/a&gt;&lt;/p&gt;
</summary><content type="html">&lt;p&gt;I started to learn a bit of Haskell and one of its features amazed me: Pattern Matching. Then, unexpectably, I notice Python 3 have them too but with a different name: Extended Tuple Unpacking. Okay, it's not exactly the same but you can get quite similar functionalities if you master it. This post is inspired by this &lt;a href="https://stackoverflow.com/questions/6967632/unpacking-extended-unpacking-and-nested-extended-unpacking"&gt;question&lt;/a&gt;&lt;/p&gt;


&lt;h1 id="pattern-what"&gt;&lt;a class="toclink" href="#pattern-what"&gt;Pattern what?&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Pattern matching is a feature of Haskell that allows us to think in terms of &lt;a href="https://stackoverflow.com/a/6957292/7690767"&gt;wholemeal programming&lt;/a&gt;, this means that we forget about the specific details of what we are working with. For example: Think in terms of first and last element instead of index 0 and index -1. This way of thinking has a very strong link with another common functional pattern: Recursive Functions&lt;/p&gt;
&lt;h1 id="is-this-a-python-or-a-haskell-thing"&gt;&lt;a class="toclink" href="#is-this-a-python-or-a-haskell-thing"&gt;Is this a Python or a Haskell thing?&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Haskell is much older than Python and there are other languages that implements it besides Haskell so we can't say it's from either. But it turned out to be something that useful that it was incorporated into Python in the &lt;a href="https://www.python.org/dev/peps/pep-3132/#id3"&gt;PEP 3132&lt;/a&gt; (In case you don't know, PEPs are the python enhancenment proposals, where all new features are asked and discussed).&lt;/p&gt;
&lt;h1 id="how-does-it-look-like"&gt;&lt;a class="toclink" href="#how-does-it-look-like"&gt;How does it look like?&lt;/a&gt;&lt;/h1&gt;
&lt;h2 id="tuple-unpacking"&gt;&lt;a class="toclink" href="#tuple-unpacking"&gt;Tuple Unpacking&lt;/a&gt;&lt;/h2&gt;
&lt;p&gt;You probably used and make yourself familiar with typica tuple unpacking&lt;/p&gt;
&lt;p&gt;Either because you did this:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="mi"&gt;3&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;4&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;5&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;or&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="mi"&gt;3&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;4&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;5&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Or this:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="mi"&gt;5&lt;/span&gt;
&lt;span class="n"&gt;b&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="mi"&gt;10&lt;/span&gt;
&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;b&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;a&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Or maybe you use it at the top of a for loop:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;names&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="s1"&gt;&amp;#39;John&amp;#39;&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="s1"&gt;&amp;#39;Peter&amp;#39;&lt;/span&gt;&lt;span class="p"&gt;]&lt;/span&gt;
&lt;span class="k"&gt;for&lt;/span&gt; &lt;span class="n"&gt;i&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;name&lt;/span&gt; &lt;span class="ow"&gt;in&lt;/span&gt; &lt;span class="nb"&gt;enumerate&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;names&lt;/span&gt;&lt;span class="p"&gt;):&lt;/span&gt;
    &lt;span class="k"&gt;print&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;f&lt;/span&gt;&lt;span class="s1"&gt;&amp;#39;{i}: {name}&amp;#39;&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;h2 id="extended-tuple-unpacking"&gt;&lt;a class="toclink" href="#extended-tuple-unpacking"&gt;Extended Tuple Unpacking&lt;/a&gt;&lt;/h2&gt;
&lt;p&gt;These are common uses of the tuple unpacking but a more powerful feature is built on top of it: The extended version, which uses the &lt;a href="https://docs.python.org/3/tutorial/controlflow.html#unpacking-argument-lists"&gt;Unpacking Argument Lists (splat)&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Lets see some examples:&lt;/p&gt;
&lt;p&gt;We will use the following list for this example:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;list_&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="mi"&gt;0&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;3&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;4&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;5&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;6&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;7&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;8&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="mi"&gt;9&lt;/span&gt;&lt;span class="p"&gt;]&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Which by the way can also be written as:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;list_&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="n"&gt;i&lt;/span&gt; &lt;span class="k"&gt;for&lt;/span&gt; &lt;span class="n"&gt;i&lt;/span&gt; &lt;span class="ow"&gt;in&lt;/span&gt; &lt;span class="nb"&gt;range&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="mi"&gt;10&lt;/span&gt;&lt;span class="p"&gt;)]&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Or even more simplified:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;list_&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="nb"&gt;list&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="nb"&gt;range&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="mi"&gt;10&lt;/span&gt;&lt;span class="p"&gt;))&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;h3 id="examples"&gt;&lt;a class="toclink" href="#examples"&gt;Examples&lt;/a&gt;&lt;/h3&gt;
&lt;p&gt;Get the first item of a list:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;rest&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;list_&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Get the last item of a list:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;rest&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;list_&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;p&gt;Get the first and the last item of a list&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;rest&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;list_&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;h1 id="caveats"&gt;&lt;a class="toclink" href="#caveats"&gt;Caveats&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Although it may seem quite simple an easy, there are lots of ways to get it wrong:&lt;/p&gt;
&lt;div class="highlight w3-round-xlarge w3-card-4"&gt;&lt;pre&gt;&lt;span&gt;&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;),&lt;/span&gt; &lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="s2"&gt;&amp;quot;XYZ&amp;quot;&lt;/span&gt;                 &lt;span class="c1"&gt;# ERROR -- too many values to unpack&lt;/span&gt;
&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;),&lt;/span&gt; &lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="s2"&gt;&amp;quot;XY&amp;quot;&lt;/span&gt;                  &lt;span class="c1"&gt;# ERROR -- need more than 1 value to unpack&lt;/span&gt;
&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;),&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;c&lt;/span&gt;&lt;span class="p"&gt;,)&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;],&lt;/span&gt;&lt;span class="s1"&gt;&amp;#39;this&amp;#39;&lt;/span&gt;       &lt;span class="c1"&gt;# ERROR -- too many values to unpack&lt;/span&gt;
&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;c&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;d&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;3&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;4&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;5&lt;/span&gt;         &lt;span class="c1"&gt;# ERROR -- two starred expressions in assignment&lt;/span&gt;
&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;),&lt;/span&gt;&lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;3&lt;/span&gt;                  &lt;span class="c1"&gt;# ERROR -- too many values to unpack&lt;/span&gt;
&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;a&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="n"&gt;b&lt;/span&gt;&lt;span class="p"&gt;),&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;c&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="mi"&gt;3&lt;/span&gt;                &lt;span class="c1"&gt;# ERROR - &amp;#39;int&amp;#39; object is not iterable&lt;/span&gt;
&lt;/pre&gt;&lt;/div&gt;


&lt;h1 id="is-there-any-logic-behind-it"&gt;&lt;a class="toclink" href="#is-there-any-logic-behind-it"&gt;Is there any logic behind it?&lt;/a&gt;&lt;/h1&gt;
&lt;p&gt;Of course there is! This is programming and for a full explanation I recommend you to read the PEP and this &lt;a href="https://stackoverflow.com/a/6968451"&gt;answer&lt;/a&gt;&lt;/p&gt;</content><category term="Haskell"></category><category term="Python"></category><category term="Tuple Unpacking"></category></entry></feed>